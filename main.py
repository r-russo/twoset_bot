import argparse
import json
import logging
import time
from logging import Formatter
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path

from twoset_bot import config
from twoset_bot.downloaders import get_downloader
from twoset_bot.uploaders import get_uploader
from twoset_bot.utils import clean_temp_files, update_video_db


def setup_logger():
    logger = logging.getLogger("twoset_bot")
    formatter = Formatter(fmt="[%(asctime)s] %(levelname)s: %(message)s")

    log_file = Path(config.log_dest) / "bot.log"
    handler = TimedRotatingFileHandler(
        filename=log_file.absolute(), when="D", interval=1, backupCount=30
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    return logger


def parse_arguments():
    parser = argparse.ArgumentParser(prog="TwoSet Bot")
    parser.add_argument(
        "-u", "--uploader", choices=["debug", "twitter"], default="twitter"
    )
    parser.add_argument(
        "-d", "--downloader", choices=["debug", "yt-dlp"], default="yt-dlp"
    )
    return parser.parse_args()


def main():
    logger = setup_logger()

    args = parse_arguments()

    logger.info("Using uploader %s", args.uploader)
    logger.info("Using downloader %s", args.downloader)

    uploader = get_uploader(args.uploader)
    downloader = get_downloader(args.downloader)
    videos_db = None

    if Path(config.last_status_filename).exists():
        with open(config.last_status_filename, "r") as f:
            last_status = json.load(f)
    else:
        last_status = {
            "db_update": 0.0,
            "post": 0,
            "mention_id": 1,
        }
    post_data = None

    clean_temp_files()

    while True:
        if (
            time.time() - last_status["db_update"] > 86400 or videos_db is None
        ):  # once a day
            videos_db = update_video_db()
            last_status["db_update"] = time.time()

        if post_data is None:
            logger.info("Preparing next post...")
            try:
                post_data = downloader.get_frame_and_caption(videos_db)
                logger.info("Got caption %s", post_data.caption)
            except Exception as e:
                logger.error("Couldn't download because of exception")
                logger.error("%s", e)

        try:
            last_status["mention_id"] = uploader.check_mentions(
                last_status["mention_id"], videos_db
            )
        except Exception as e:
            logger.error("Couldn't get mentions because of error:")
            logger.error("%s", e)

        if time.time() - last_status["post"] > config.update_interval:
            # time to post!
            logging.info("Preparing to make a post")
            if post_data is not None:
                if len(post_data.caption) > 280:
                    logger.info("Trimming caption")
                    post_data.caption = post_data.caption[:280]
                    post_data.caption = post_data.caption[
                        : -post_data.caption[::-1].find(" ") - 1
                    ]
                try:
                    post_data = None
                    last_status["post"] = time.time()
                    uploader.post(post_data)
                    logger.info("Done")
                except Exception as e:
                    logger.error("Couldn't post because of exception")
                    logger.error("%s", e)

            clean_temp_files()

        with open(config.last_status_filename, "w") as f:
            json.dump(last_status, f)
        time.sleep(30)


if __name__ == "__main__":
    main()
