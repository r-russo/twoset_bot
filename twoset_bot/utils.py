import logging
from pathlib import Path

from googleapiclient.discovery import build

from twoset_bot import config, credentials
from twoset_bot.types import VideoInfo


def update_video_db() -> list[VideoInfo]:
    videos_db: list[VideoInfo] = []
    logger = logging.getLogger("twoset_bot")
    logger.info("Connecting to Google API...")
    youtube = build("youtube", "v3", developerKey=credentials.google_api_key)
    logger.info("Getting videos list...")
    req = youtube.playlistItems().list(part="snippet", playlistId=config.playlist_id)
    while req is not None:
        logger.info("Executing request...")
        response = req.execute()
        logger.info("Updating videos database...")
        for i in response["items"]:
            title = i["snippet"]["title"]
            video_id = i["snippet"]["resourceId"]["videoId"]
            if video_id in config.blacklist_ids:
                logger.info("Blacklisted `%s`", title)
                continue
            videos_db.append(VideoInfo(title, video_id))
            logger.info("Found %s", videos_db[-1])

        req = youtube.playlistItems().list_next(req, response)

    logger.info("Found %d videos in playlist", len(videos_db))
    return videos_db


def clean_temp_files():
    logger = logging.getLogger("twoset_bot")
    logger.info("Cleaning temporary files")
    tmp_dir = Path(config.tmp_dest).absolute()
    if tmp_dir.exists():
        for item in tmp_dir.iterdir():
            item.unlink()
