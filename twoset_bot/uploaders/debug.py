import logging
from typing import List

from twoset_bot.types import PostData, VideoInfo
from twoset_bot.uploaders import Uploader

logger = logging.getLogger("twoset_bot")


class UploaderDebug(Uploader):
    def __init__(self):
        logger.info("Initializing debug uploader")

    def post(self, post_data: PostData):
        logging.info("Posting %s from %s", post_data.caption, post_data.url)

    def check_mentions(self, last_mention_id: int, videos_db: List[VideoInfo]) -> int:
        logging.info("Checking mentions...")
        return last_mention_id + 1
