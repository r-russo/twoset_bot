from typing import List, Protocol

from twoset_bot.types import PostData, VideoInfo


class Uploader(Protocol):
    def post(self, post_data: PostData) -> None:
        ...

    def check_mentions(self, last_mention_id: int, videos_db: List[VideoInfo]) -> int:
        ...


def get_uploader(uploader: str, *args, **kwargs) -> Uploader:
    from twoset_bot.uploaders.debug import UploaderDebug
    from twoset_bot.uploaders.twitter import UploaderTwitter

    if uploader == "debug":
        return UploaderDebug(*args, **kwargs)
    elif uploader == "twitter":
        return UploaderTwitter(*args, **kwargs)
    else:
        raise NotImplemented("Uploader %s not implemented", uploader)
