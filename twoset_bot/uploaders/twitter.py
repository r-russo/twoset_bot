import logging
from pathlib import Path
from random import randint
from typing import List

from twoset_bot import config
from twoset_bot.types import PostData, VideoInfo
from twoset_bot.uploaders import Uploader

try:
    from twoset_bot import credentials
except ImportError:
    print(
        "credentials.py not accessible. Make sure the file is created as mentioned in the readme file"
    )

import tweepy

logger = logging.getLogger("twoset_bot")


class UploaderTwitter(Uploader):
    def __init__(self):
        logger.info("Initializing Twitter uploader")

        logger.info("Connecting to twitter...")
        auth = tweepy.OAuth1UserHandler(
            credentials.twitter_api_key,
            credentials.twitter_api_secret,
            credentials.twitter_access_token,
            credentials.twitter_token_secret,
        )
        self.api = tweepy.API(auth, wait_on_rate_limit=True)

        try:
            tweepy_credentials = self.api.verify_credentials()
        except tweepy.Unauthorized as e:
            logger.error("Unauthorized. Check credentials")
            raise e

        self.user_id = tweepy_credentials.id

        logger.info("Connected as %s with id %d", tweepy_credentials.name, self.user_id)

    def post(self, post_data: PostData):
        frame_path = Path(config.tmp_dest) / "frame.png"
        media = self.api.media_upload(frame_path)
        print(media)
        status = self.api.update_status(
            post_data.caption, media_ids=[media.media_id_string]
        )
        self.api.update_status(
            f"@twosetbot Source: {post_data.url}", in_reply_to_status_id=status.id
        )
        logger.info("Posted tweet and reply with caption %s", post_data.caption)

    def check_mentions(self, last_mention_id: int, videos_db: List[VideoInfo]) -> int:
        for tweet in self.api.mentions_timeline(since_id=last_mention_id):
            user = tweet.user.id
            last_mention_id = max(tweet.id, last_mention_id)
            if user == self.user_id:
                continue

            if any(
                keyword in tweet.text.lower() for keyword in config.mention_keywords
            ):
                logger.info("Found mention by %s" % tweet.user.name)
                random_ix = randint(0, len(videos_db) - 1)
                video_id = videos_db[random_ix].id
                video = f"https://www.youtube.com/watch?v={video_id}"
                message = (
                    f"Hi @{tweet.user.screen_name}! Here's some TwoSet for you: {video}"
                )
                self.api.update_status(status=message, in_reply_to_status_id=tweet.id)

        return last_mention_id
