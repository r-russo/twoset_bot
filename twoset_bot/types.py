from dataclasses import dataclass


@dataclass(frozen=True)
class VideoInfo:
    title: str
    id: str


@dataclass
class PostData:
    caption: str
    url: str
