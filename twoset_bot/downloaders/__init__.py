from typing import Protocol

from twoset_bot.types import PostData, VideoInfo


class Downloader(Protocol):
    def get_frame_and_caption(self, videos_db: list[VideoInfo]) -> PostData:
        ...


def get_downloader(downloader: str) -> Downloader:
    from twoset_bot.downloaders.debug import DownloaderDebug
    from twoset_bot.downloaders.yt_dlp import DownloaderYtDlp

    if downloader == "debug":
        return DownloaderDebug()
    elif downloader == "yt-dlp":
        return DownloaderYtDlp()
    else:
        raise NotImplemented("Downloader %s not implemented", downloader)
