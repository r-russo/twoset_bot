import logging
from random import randint

from twoset_bot.downloaders import Downloader
from twoset_bot.types import PostData, VideoInfo

logger = logging.getLogger("twoset_bot")


class DownloaderDebug(Downloader):
    def get_frame_and_caption(self, videos_db: list[VideoInfo]) -> PostData:
        random_ix = randint(0, len(videos_db) - 1)
        video = videos_db[random_ix]
        video_id = video.id
        caption = "Placeholder"
        url = f"https://www.youtube.com/watch?v={video_id}"

        return PostData(caption, url)
