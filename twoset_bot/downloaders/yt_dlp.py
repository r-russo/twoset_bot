import logging
import re
from pathlib import Path
from random import randint, random
from typing import Optional

import ffmpeg
from yt_dlp import YoutubeDL

from twoset_bot import config
from twoset_bot.downloaders import Downloader
from twoset_bot.types import PostData, VideoInfo

logger = logging.getLogger("twoset_bot")


class DownloaderYtDlp(Downloader):
    def __init__(self):
        self.tmp_dir = Path(config.tmp_dest).absolute()
        self.ydl_opts = {
            "format": "bestvideo[ext=mp4]",
            "writesubtitles": True,
            "writeautomaticsub": True,
            "subtitleslangs": ["en"],
            "outtmpl": str(self.tmp_dir) + "/%(id)s.%(ext)s",
            "logger": logger,
        }

    def get_frame_and_caption(self, videos_db: list[VideoInfo]) -> PostData:
        while True:
            try:
                video_id, video_fname, subs_fname = self._download_video(videos_db)
            except VideoDownloadException as e:
                logging.warn("Skipping current video. Raised exception:")
                logging.warn("%s", e)
                continue

            timestamp = self._get_random_frame(video_fname)
            caption = self._parse_subs(subs_fname)
            url = f"https://www.youtube.com/watch?v={video_id}&t={timestamp}s"

            return PostData(caption, url)

    def _download_video(
        self,
        videos_db: list[VideoInfo],
        video_id: Optional[str] = None,
    ) -> tuple[str, Path, Path]:
        if video_id is None:
            random_ix = randint(0, len(videos_db) - 1)
            video = videos_db[random_ix]
            video_id = video.id
            title = video.title
            logger.info("Downloading video id %s titled %s", video_id, title)
        else:
            logger.info("Downloading video id %s", video_id)

        url = f"https://www.youtube.com/watch?v={video_id}"
        with YoutubeDL(self.ydl_opts) as ydl:
            ydl.download([url])

        video_fname = self.tmp_dir / f"{video_id}.mp4"
        subs_fname = self.tmp_dir / f"{video_id}.en.vtt"
        if not video_fname.exists():
            raise VideoDownloadException(
                "Video %s wasn't downloaded properly" % video_id
            )
        if not subs_fname.exists():
            raise VideoDownloadException("Subs for %s not available" % video_id)

        return video_id, video_fname, subs_fname

    def _get_random_frame(self, fname: Path) -> int:
        probe = ffmpeg.probe(fname)
        duration = float(probe["streams"][0]["duration"])
        pos = random() * duration
        width = probe["streams"][0]["width"]
        logger.info("Extracting frame from %.2f", pos)
        (
            ffmpeg.input(fname, ss=pos, y=None)
            .filter("scale", width, -1)
            .output(str((self.tmp_dir / "frame.png").absolute()), vframes=1)
            .run()
        )

        return int(pos)

    def _parse_subs(self, fname: Path) -> str:
        logger.info("Getting subs from random timestamp...")
        with open(fname, "r") as f:
            lines = f.readlines()

        # Remove timestamps, header and empty new lines
        subs = [x for x in lines if x != "\n" and "-->" not in x][3:]

        while True:
            caption = subs[randint(0, len(subs) - 1)].strip("\n")

            # Remove tags in old automatic subs
            clean_tags = re.compile("<.*?>")
            caption = re.sub(clean_tags, "", caption)
            if not caption.isspace() and len(caption) > 0:  # Avoid empty titles
                break

        return caption


class VideoDownloadException(Exception):
    pass
